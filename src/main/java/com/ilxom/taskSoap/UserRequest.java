package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userRequest", propOrder = {
        "password",
        "userName",
        "parameters",
        "amount"
})
public class UserRequest {
    @XmlElement(required = true)
    protected String password;

    @XmlElement(required = true)
    protected String userName;

    @XmlElement(required = true)
    protected List<GenericParam<?>> parameters;

    @XmlElement(required = true)
    protected long amount;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the name property.
     *
     * @param password
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the name property.
     *
     * @param userName
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link GenericParam }
     *
     */
    public List<GenericParam<?>> getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the name property.
     *
     * @param parameters
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParameters(List<GenericParam<?>> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */


    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
