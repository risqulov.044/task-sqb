package com.ilxom.taskSoap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "paramKey",
        "paramValue"
})
@XmlRootElement(name = "GenericParam")
public class GenericParam<T> {

    private String paramKey;

    private String paramValue;

//    public GenericParam(String key, String value) {
//        this.paramKey = key;
//        this.paramValue = value;
//    }


    /**
     * Gets the value of the employmentMode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getParamKey() {
        return paramKey;
    }

    /**
     * Sets the value of the employmentMode property.
     *
     * @param paramKey
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    /**
     * Gets the value of the employmentMode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * Sets the value of the employmentMode property.
     *
     * @param paramValue
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
