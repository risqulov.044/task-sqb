package com.ilxom.taskSoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskSoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskSoapApplication.class, args);
	}
}
