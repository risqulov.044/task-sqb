package com.ilxom.taskSoap.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
    private String message;
}
