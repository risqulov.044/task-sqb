package com.ilxom.taskSoap.utils;

import lombok.Getter;

@Getter
public enum Code {

    SUCCESS,
    DATA_NOT_FOUND,
    INVALID_DATA
}
