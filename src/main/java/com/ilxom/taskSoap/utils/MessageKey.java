package com.ilxom.taskSoap.utils;

import lombok.Getter;

@Getter
public class MessageKey {

    public static final String KEY_NOT_FOUND=  "key.not.found";
    public static final String TASK_SOAP=  "task.soap";
    public static final String USER_NAME_OR_PASSWORD_MISTAKE=  "userName.or.password.mistake";
    public static final String USER_NAME_ALREADY_EXIST = "user.name.already.exist";
    public static final String PHONE_NUMBER_ALREADY_EXIST = "phone.number.already.exist";
    public static final String USER_NAME_NOT_FOUND=  "userName.not.found";
    public static final String PASSWORD_NOT_FOUND=  "password.not.found";
    public static final String INVALID_ARGUMENT = "invalid.argument";
}
