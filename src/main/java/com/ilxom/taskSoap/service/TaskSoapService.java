package com.ilxom.taskSoap.service;

import com.ilxom.taskSoap.*;
import com.ilxom.taskSoap.RegisterResponse;

public interface TaskSoapService {

    RegisterResponse register(UserRequest userRequest);

    GetInformationResult getInformation(GetInformationArguments arguments);

    PerformTransactionResult performTransaction(PerformTransactionArguments arguments);

    ChangePasswordResult changePassword(ChangePasswordArguments arguments);
}
