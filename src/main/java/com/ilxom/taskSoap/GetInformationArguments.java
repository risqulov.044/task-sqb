package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "genericArguments",
        "parameters",
        "serviceId"
})
@XmlRootElement(name = "GetInformationArguments")
public class GetInformationArguments {

    @XmlElement(required = true)
    protected GenericArguments genericArguments;

    @XmlElement(required = true)
    protected List<GenericParam<?>> parameters;

    @XmlElement(required = true)
    protected long serviceId;

    public GenericArguments getGenericArguments() {
        return genericArguments;
    }

    public void setGenericArguments(GenericArguments genericArguments) {
        this.genericArguments = genericArguments;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link GenericParam }
     *
     */
    public List<GenericParam<?>> getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the name property.
     *
     * @param parameters
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParameters(List<GenericParam<?>> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }
}
