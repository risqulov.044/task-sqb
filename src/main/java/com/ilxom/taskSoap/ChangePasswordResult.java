package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePasswordResult", propOrder = {
        "genericResult"
})
@XmlRootElement
public class ChangePasswordResult {
    @XmlElement(required = true)
    protected GenericResult genericResult;


    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public GenericResult getGenericResult() {
        return genericResult;
    }

    /**
     * Sets the value of the name property.
     *
     * @param genericResult
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGenericResult(GenericResult genericResult) {
        this.genericResult = genericResult;
    }
}
