package com.ilxom.taskSoap.mapper;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ilxom.taskSoap.entity.Customer;
import com.ilxom.taskSoap.RegisterResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", imports = {Customer.class, RegisterResponse.class})
public interface TaskSoapMapper {

    ObjectMapper mapper = new ObjectMapper();

//    @Mapping(target = "parameters", source = "taskSoap", qualifiedByName = "mapFields")
//    RegisterResponse toResponse(TaskSoap taskSoap);


//    @Named("mapFields")
//    default List<GenericParam> mapFields(TaskSoap taskSoap) {
//        return ofNullable(taskSoap.getParameters())
//                .flatMap(json -> Try.of(() -> mapper.readValue(json, new TypeReference<List<GenericParam>>() {
//                        })).toJavaOptional()
//                ).orElse(null);
//    }
}
