package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "genericArguments",
        "amount",
        "parameters",
        "serviceId",
        "transactionId",
        "transactionTime"
})
@XmlRootElement(name = "PerformTransactionArguments")
public class PerformTransactionArguments {

    @XmlElement(required = true)
    protected GenericArguments genericArguments;

    @XmlElement(required = true)
    protected long amount;

    @XmlElement(required = true)
    protected List<GenericParam<?>> parameters;

    @XmlElement(required = true)
    protected long serviceId;

    @XmlElement(required = true)
    protected long transactionId;

    @XmlElement(required = true)
    protected Date transactionTime;

    public GenericArguments getGenericArguments() {
        return genericArguments;
    }

    public void setGenericArguments(GenericArguments genericArguments) {
        this.genericArguments = genericArguments;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link GenericParam }
     *
     */
    public List<GenericParam<?>> getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the name property.
     *
     * @param parameters
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParameters(List<GenericParam<?>> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }
}
