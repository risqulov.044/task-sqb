package com.ilxom.taskSoap.exection;

import com.ilxom.taskSoap.utils.Code;
import lombok.Getter;

import static com.ilxom.taskSoap.utils.Code.DATA_NOT_FOUND;
import static com.ilxom.taskSoap.utils.MessageKey.*;

@Getter
public class BadRequestAlertException extends RuntimeException {

    private final Code code;

    public BadRequestAlertException(String message, Code code) {
        super(message);
        this.code = code;
    }

    public static BadRequestAlertException keyNotFound() {
        return new BadRequestAlertException(KEY_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException customerNotFound() {
        return new BadRequestAlertException(KEY_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException userNameNotFound() {
        return new BadRequestAlertException(USER_NAME_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException passwordNotFound() {
        return new BadRequestAlertException(PASSWORD_NOT_FOUND, DATA_NOT_FOUND);
    }
}
