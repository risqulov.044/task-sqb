package com.ilxom.taskSoap;

import com.ilxom.taskSoap.service.TaskSoapService;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class CountryEndpoint {
	private static final String NAMESPACE_URI = "http://uws.provider.com";

    private final TaskSoapService taskSoapService;


	public CountryEndpoint(TaskSoapService taskSoapService) {
		this.taskSoapService = taskSoapService;
	}


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "userRequest")
	@ResponsePayload
	public RegisterResponse add(@RequestPayload UserRequest request){
		return taskSoapService.register(request);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetInformationArguments")
	@ResponsePayload
	public GetInformationResult getInformation(@RequestPayload GetInformationArguments arguments){
		return taskSoapService.getInformation(arguments);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "PerformTransactionArguments")
	@ResponsePayload
	public PerformTransactionResult performTransaction(@RequestPayload PerformTransactionArguments arguments){
		return taskSoapService.performTransaction(arguments);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "ChangePasswordArguments")
	@ResponsePayload
	public ChangePasswordResult performTransaction(@RequestPayload ChangePasswordArguments arguments){
		return taskSoapService.changePassword(arguments);
	}
}
