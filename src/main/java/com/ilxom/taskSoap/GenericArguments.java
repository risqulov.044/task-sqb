package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericArguments", propOrder = {
        "password",
        "userName"
})
public class GenericArguments {
    @XmlElement(required = true)
    protected String password;

    @XmlElement(required = true)
    protected String userName;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the name property.
     *
     * @param password
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the name property.
     *
     * @param userName
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
