package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetInformationResult", propOrder = {
        "genericResult",
        "parameters"
})
@XmlRootElement
public class GetInformationResult {
    @XmlElement(required = true)
    protected GenericResult genericResult;

    @XmlElement(required = true)
    protected List<GenericParam<?>> parameters;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public GenericResult getGenericResult() {
        return genericResult;
    }

    /**
     * Sets the value of the name property.
     *
     * @param genericResult
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGenericResult(GenericResult genericResult) {
        this.genericResult = genericResult;
    }


    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link GenericParam }
     *
     */
    public List<GenericParam<?>> getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the name property.
     *
     * @param parameters
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParameters(List<GenericParam<?>> parameters) {
        this.parameters = parameters;
    }
}
