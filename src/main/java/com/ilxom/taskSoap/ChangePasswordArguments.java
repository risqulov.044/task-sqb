package com.ilxom.taskSoap;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "genericArguments",
        "newPassword"
})
@XmlRootElement(name = "ChangePasswordArguments")
public class ChangePasswordArguments {

    @XmlElement(required = true)
    protected GenericArguments genericArguments;

    @XmlElement(required = true)
    protected String newPassword;

    public GenericArguments getGenericArguments() {
        return genericArguments;
    }

    public void setGenericArguments(GenericArguments genericArguments) {
        this.genericArguments = genericArguments;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
