package com.ilxom.taskSoap.repository;

import com.ilxom.taskSoap.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskSoapRepository extends JpaRepository<Customer, Long> {

    Optional<Customer> findByPhoneNumber(String phoneNumber);

    Optional<Customer> findByWalletNumber(String walletNumber);

    Boolean existsByUserName(String userName);

    Boolean existsByUserNameAndPassword(String userName, String password);

    Optional<Customer> findByUserNameAndPassword(String userName, String password);

    Boolean existsByPhoneNumber(String phoneNumber);
}
