package com.ilxom.taskSoap.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ilxom.taskSoap.*;
import com.ilxom.taskSoap.entity.Customer;
import com.ilxom.taskSoap.exection.BadRequestAlertException;
import com.ilxom.taskSoap.mapper.TaskSoapMapper;
import com.ilxom.taskSoap.RegisterResponse;
import com.ilxom.taskSoap.repository.TaskSoapRepository;
import com.ilxom.taskSoap.service.TaskSoapService;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ilxom.taskSoap.utils.Code.INVALID_DATA;
import static com.ilxom.taskSoap.utils.MessageKey.*;
import static java.util.Optional.ofNullable;

@Log4j2
@Service
@RequiredArgsConstructor
public class TaskSoapServiceImpl implements TaskSoapService {


    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TaskSoapRepository repository;
    private final TaskSoapMapper mapper;


    @Override
    public RegisterResponse register(UserRequest userRequest) {

        Boolean existsByUserName = repository.existsByUserName(userRequest.getUserName());
        if (existsByUserName)
            throw new BadRequestAlertException(USER_NAME_ALREADY_EXIST, INVALID_DATA);

        String phoneNumber = userRequest.getParameters().stream().filter(param -> param.getParamKey().equals("phoneNumber"))
                .findFirst().map(GenericParam::getParamValue)
                .orElseThrow(BadRequestAlertException::keyNotFound);

        checkPhoneNumber(phoneNumber);

        if (phoneNumber.startsWith("+"))
            phoneNumber = phoneNumber.substring(1);

        Boolean existsByPhoneNumber = repository.existsByPhoneNumber(phoneNumber);
        if (existsByPhoneNumber)
            throw new BadRequestAlertException(PHONE_NUMBER_ALREADY_EXIST, INVALID_DATA);

        RandomWalletNumber randomWalletNumber = new RandomWalletNumber();
        String walletNumber = randomWalletNumber.generate("999", 16);

        validateCreditCardNumber(walletNumber);

        Customer taskSoap = new Customer();
        taskSoap.setPassword(userRequest.getPassword());
        taskSoap.setUserName(userRequest.getUserName());
        taskSoap.setAmount(0L);
        taskSoap.setPhoneNumber(phoneNumber);
        taskSoap.setWalletNumber(walletNumber);
        repository.save(taskSoap);
        RegisterResponse response = new RegisterResponse();
        response.setMessage("Successfully");
        return response;
    }

    @Override
    public GetInformationResult getInformation(GetInformationArguments arguments) {

        String userName = arguments.getGenericArguments().getUserName();
        String password = arguments.getGenericArguments().getPassword();

        Boolean byUserNameAndPassword = repository.existsByUserNameAndPassword(userName, password);
        if (!byUserNameAndPassword)
            throw new BadRequestAlertException(USER_NAME_OR_PASSWORD_MISTAKE, INVALID_DATA);

        String parameters = Try.of(() -> objectMapper.writeValueAsString(arguments.getParameters()))
                .onFailure(log::error)
                .getOrElse("");

        Optional<String> phoneNumber = arguments.getParameters().stream().filter(genericParam -> genericParam.getParamKey().equals("phoneNumber"))
                .findFirst().map(GenericParam::getParamValue);

        Optional<String> walletNumber = arguments.getParameters().stream().filter(genericParam -> genericParam.getParamKey().equals("walletNumber"))
                .findFirst().map(GenericParam::getParamValue);

        if (walletNumber.isPresent()) {
            repository.findByWalletNumber(walletNumber.get()).orElseThrow(BadRequestAlertException::keyNotFound);
        } else if (phoneNumber.isPresent()) {
            String phone = phoneNumber.get();
            checkPhoneNumber(phone);
            if (phone.startsWith("+"))
                phone = phone.substring(1);
            repository.findByPhoneNumber(phone).orElseThrow(BadRequestAlertException::keyNotFound);
        } else {
            throw new BadRequestAlertException(INVALID_ARGUMENT, INVALID_DATA);
        }

        List<GenericParam<?>> genericParams = ofNullable(parameters)
                .flatMap(json -> Try.of(() -> objectMapper.readValue(json, new TypeReference<List<GenericParam<?>>>() {
                        })).toJavaOptional()
                ).orElse(null);

        long a = (long) (Math.random() * (124500000 - 1000 + 1) + 1000);

        GenericParam<?> limitParam = new GenericParam<>();
        limitParam.setParamKey("limitAmount");
        limitParam.setParamValue(String.valueOf(a));
        genericParams.add(limitParam);

        GetInformationResult result = new GetInformationResult();
        GenericResult genericResult = new GenericResult();
        genericResult.setStatus(0);
        genericResult.setErrorMsg("");
        genericResult.setTimeStamp(Timestamp.from(Instant.now()));
        result.setGenericResult(genericResult);
        result.setParameters(genericParams);
        return result;
    }

    @Override
    public PerformTransactionResult performTransaction(PerformTransactionArguments arguments) {

        Customer customer;

        String userName = arguments.getGenericArguments().getUserName();
        String password = arguments.getGenericArguments().getPassword();

        Boolean byUserNameAndPassword = repository.existsByUserNameAndPassword(userName, password);
        if (!byUserNameAndPassword)
            throw new BadRequestAlertException(USER_NAME_OR_PASSWORD_MISTAKE, INVALID_DATA);

        String parameters = Try.of(() -> objectMapper.writeValueAsString(arguments.getParameters()))
                .onFailure(log::error)
                .getOrElse("");

        Optional<String> phoneNumber = arguments.getParameters().stream().filter(genericParam -> genericParam.getParamKey().equals("phoneNumber"))
                .findFirst().map(GenericParam::getParamValue);

        Optional<String> walletNumber = arguments.getParameters().stream().filter(genericParam -> genericParam.getParamKey().equals("walletNumber"))
                .findFirst().map(GenericParam::getParamValue);

        String limit = arguments.getParameters().stream().filter(genericParam -> genericParam.getParamKey().equals("limitAmount"))
                .findFirst().map(GenericParam::getParamValue)
                .orElseThrow(BadRequestAlertException::keyNotFound);

        if (walletNumber.isPresent()) {
            customer = repository.findByWalletNumber(walletNumber.get()).orElseThrow(BadRequestAlertException::keyNotFound);
        } else if (phoneNumber.isPresent()) {
            String phone = phoneNumber.get();
            checkPhoneNumber(phone);
            if (phone.startsWith("+"))
                phone = phone.substring(1);
            customer = repository.findByPhoneNumber(phone).orElseThrow(BadRequestAlertException::keyNotFound);
        } else {
            throw new BadRequestAlertException(INVALID_ARGUMENT, INVALID_DATA);
        }

        List<GenericParam<?>> genericParams = ofNullable(parameters)
                .flatMap(json -> Try.of(() -> objectMapper.readValue(json, new TypeReference<List<GenericParam<?>>>() {
                        })).toJavaOptional()
                ).orElse(null);

        long amount = arguments.getAmount();
        long limitAmount = Long.parseLong(limit);
        if (amount>limitAmount)
            throw new BadRequestAlertException(INVALID_ARGUMENT, INVALID_DATA);
        customer.setAmount(customer.getAmount()+amount);
        repository.save(customer);

        GenericParam<?> balance = new GenericParam<>();
        balance.setParamKey("balance");
        balance.setParamValue(customer.getAmount().toString());
        genericParams.add(balance);

        PerformTransactionResult result = new PerformTransactionResult();
        GenericResult genericResult = new GenericResult();
        genericResult.setStatus(0);
        genericResult.setErrorMsg("");
        genericResult.setTimeStamp(Timestamp.from(Instant.now()));
        result.setGenericResult(genericResult);
        result.setParameters(genericParams);

        return result;
    }

    @Override
    public ChangePasswordResult changePassword(ChangePasswordArguments arguments) {

        String userName = arguments.getGenericArguments().getUserName();
        String password = arguments.getGenericArguments().getPassword();

        Boolean byUserNameAndPassword = repository.existsByUserNameAndPassword(userName, password);
        if (!byUserNameAndPassword)
            throw new BadRequestAlertException(USER_NAME_OR_PASSWORD_MISTAKE, INVALID_DATA);

        String newPassword = arguments.getNewPassword();

        Customer customer = repository.findByUserNameAndPassword(userName, password).orElseThrow(BadRequestAlertException::userNameNotFound);
        customer.setPassword(newPassword);
        repository.save(customer);
        ChangePasswordResult result =new ChangePasswordResult();
        GenericResult genericResult = new GenericResult();
        genericResult.setStatus(0);
        genericResult.setErrorMsg("");
        genericResult.setTimeStamp(Timestamp.from(Instant.now()));
        result.setGenericResult(genericResult);
        return result;
    }

    private void checkPhoneNumber(String phoneNumber) {

        Pattern p = Pattern.compile("(\\+998)[0-9]{2}[0-9]{7}");
        Matcher m = p.matcher(phoneNumber);
        if (!m.matches())
            throw new RuntimeException("PhoneNumber not valid");
    }

    private void validateCreditCardNumber(String str) {

        int[] ints = new int[str.length()];
        for (int i = 0; i < str.length(); i++) {
            ints[i] = Integer.parseInt(str.substring(i, i + 1));
        }
        for (int i = ints.length - 2; i >= 0; i = i - 2) {
            int j = ints[i];
            j = j * 2;
            if (j > 9) {
                j = j % 10 + 1;
            }
            ints[i] = j;
        }
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
        }
        if (sum % 10 == 0) {
            System.out.println(str + " is a valid walletNumber");
        } else {
            System.out.println(str + " is an invalid walletNumber");
        }
    }
}
